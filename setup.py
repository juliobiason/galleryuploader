from distutils.core import setup
setup(name='gup',
        version='0.2.3',
        description='Gallery uploader',
        author='Julio Biason',
        author_email='slow@slowhome.org',
        url='http://slowhome.org/projects/gup',
        packages=['guplib'],
        scripts=['gup.py'],
        license='GPL',
        )

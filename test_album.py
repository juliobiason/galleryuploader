# This file is part of GUP.
#
# GUP is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any
# later version.
#
# GUP is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with GUP; if not, write to the Free Software Foundation, Inc., 51
# Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#
# Copyright (C) 2007 Julio Biason

import logging
from albums import AlbumList

logging.basicConfig(level=logging.DEBUG)

album = AlbumList()
for line in file('album.list'):
    line   = line.strip()
    fields = line.split(' : ')

    id     = int(fields[0])
    parent = int(fields[1])
    name   = ''.join(fields[2:])

    print 'ID:', id, '- Name:', name, '- Parent:', parent
    album.add(id, name, parent)

print 80 * '-'
print 'Orphans:'
album.orphans()
print 80 * '-'
for (level, name, id) in album.tree():
    print '  ' * level, name

album.save('original')

print 80 * '-'
other = AlbumList()
other.load('original')
for (level, name, id) in album.tree():
    print '---' * level, name, id

print 80 * '-'
search = album.search_name('10')
for (level, name, id) in search.tree():
    print '%s %s (%s)' % ('  ' * level, name, id)

print 80 * '-'
print 'Search ID'
search = album.search_id(3239)
for (level, name, id) in search.tree():
    print '%s %s (%s)' % ('  ' * level, name, id)

print 80 * '-'
print 'Search ID'
search = album.search_id(4646)
for (level, name, id) in search.tree():
    print '%s %s (%s)' % ('  ' * level, name, id)

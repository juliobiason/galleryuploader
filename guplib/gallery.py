# This file is part of GUP.
#
# GUP is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any
# later version.
#
# GUP is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along
# with GUP; if not, write to the Free Software Foundation, Inc., 51
# Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#
# Copyright (C) 2007 Julio Biason

'''Classes used to talk to Web Gallery 2.0'''
__revision__ = 0.1

import logging
import urllib2
import cookielib
import mimetypes

# Exceptions
class GalleryError(Exception):
    '''Generic error in the request'''
    def __str__(self):
        if self.message:
            return '%s: %s' % (self.__doc__, self.message)
        else:
            return self.__doc__

class ConnectionError(GalleryError):
    '''Connection error.'''

class MajorVersionInvalidError(GalleryError):
    '''The protocol major version the client is using is not supported.'''

class MinorVersionInvalidError(GalleryError):
    '''The protocol minor version the client is using is not supported.'''

class ProtocolFormatInvalidError(GalleryError):
    '''The format of the protocol version string the client sent
    in the request is invalid.'''

class MissingProtocolVersionError(GalleryError):
    '''The request did not contain the required protocol_version key.'''

class InvalidPasswordError(GalleryError):
    '''The password and/or username the client send in the request
    is invalid.'''

class MissingLoginError(GalleryError):
    '''The client used the login command in the request but failed
    to include either the username or password (or both) in the
    request.'''

class InvalidCommandError(GalleryError):
    '''The value of the cmd key is not valid.'''

class NoPermissionError(GalleryError):
    '''The user does not have permission to add an item to the
    gallery.'''

class NoFilenameError(GalleryError):
    '''No filename was specified.'''

class PhotoUploadError(GalleryError):
    '''The file was received, but could not be processed or
    added to the album.'''

class NoWritePermissionError(GalleryError):
    '''No write permission to destination album.'''

class NoViewPermissionError(GalleryError):
    '''No view permission for this image.'''

class NoAlbumPermissionError(GalleryError):
    '''A new album could not be created because the user does
    not have permission to do so.'''

class AlbumCreateError(GalleryError):
    '''A new album could not be created, for a different reason
    (name conflict).'''

class MoveAlbumError(GalleryError):
    '''The album could not be moved.'''

class ImageRotateError(GalleryError):
    '''The image could not be rotated'''

def _multipart(boundary, arguments, file_info):
    '''Generates the body of a multipart data'''
    parts = []
    for key, value in arguments.iteritems():
        assert '"' not in key, 'Key cannot contain " (double-quotes).'
        assert boundary not in value, (
            'Multipart encapsulation failure: '
            'boundary found in form data.')

        logging.debug('Adding "%s"...', key)
        parts.append('--%s' % boundary)
        parts.append('Content-disposition: form-data; name="%s"' % key)
        parts.append('')
        parts.append(value)

    if file_info is not None:
        key, filename = file_info
        assert '"' not in key, 'Key cannot contain " (double-quotes).'

        content_type = mimetypes.guess_type(filename)[0] or \
                'application/octet-stream'

        logging.debug('Adding file "%s" (%s) to "%s"...' %
                (filename, content_type, key))
        parts.append('--%s' % (boundary,))
        parts.append('Content-disposition: form-data; ' + \
                'name="%s"; filename="%s"' %
                (key, filename))
        parts.append('Content-Type: %s' % content_type)
        parts.append('Content-Transfer-Encoding: base64')
        parts.append('')

        image = file(filename, 'rb')
        try:
            contents = image.read()
        finally:
            image.close()

        assert boundary not in contents, (
            'Multipart encapsulation failure: '
            'boundary found in form data.')

        parts.append(contents)

    parts.append('--%s--' % boundary)

    return '\r\n'.join(parts)

def _yesno(boolean):
    if boolean:
        return 'yes'
    else:
        return 'no'

# Main class
class Gallery(object):
    '''Gallery interaction class'''
    def __init__(self, url, user, password):
        '''Class initiator.

        url      - Gallery installation URL
        user     - login user
        password - user passowrd'''

        self.logged   = False
        self.cookie   = None
        self.url      = url + '/main.php'
        self.user     = user
        self.password = password
        self.last_authtoken = ''

        # maps return codes to the exceptions
        self._return_codes = {
                101: MajorVersionInvalidError,
                102: MinorVersionInvalidError,
                103: ProtocolFormatInvalidError,
                104: MissingProtocolVersionError,
                201: InvalidPasswordError,
                202: MissingLoginError,
                301: InvalidCommandError,
                401: NoPermissionError,
                402: NoFilenameError,
                403: PhotoUploadError,
                404: NoWritePermissionError,
                405: NoViewPermissionError,
                501: NoAlbumPermissionError,
                502: AlbumCreateError,
                503: MoveAlbumError,
                504: ImageRotateError
                }

    def request(self, command, version, arguments, file_info = None):
        '''Send a request to the remote server. Returns a dictionary with
        the resulting variables.

        command - the command to be send
        version - command version
        arguments - dictionary with the arguments to the command
        file_info - a tuple with the field name and the filename to be added
          in the body'''
        if not self.logged and not command == 'login':
            # hate those hardcoded options
            self.login()

        logging.debug('Opening request with "%s"', self.url)
        boundary = '------GUP_Boundary'

        # those are the default fields
        data = {
                'g2_controller'            : 'remote:GalleryRemote',
                'g2_form[cmd]'             : command,
                'g2_form[protocol_version]': str(version),
                'g2_authToken'             : self.last_authtoken
                }
        data.update(arguments)

        request = urllib2.Request(self.url)
        request.add_header('User-agent', 'GUP %s' % (__revision__))
        request.add_header('Content-type',
                'multipart/form-data; boundary=%s' % boundary)
        request.add_header('Accept', 'text/plain')
        if self.cookie is not None:
            self.cookie.add_cookie_header(request)
        else:
            cookiejar = cookielib.CookieJar()
            cookie_opener = urllib2.build_opener(
                    urllib2.HTTPCookieProcessor(cookiejar))
            urllib2.install_opener(cookie_opener)

        request.add_data(_multipart(boundary, data, file_info))

        response = urllib2.urlopen(request)

        if self.cookie is None:
            self.cookie = cookiejar

        data = response.read()
        logging.debug('== DATA ==')
        logging.debug(data)
        logging.debug('== /DATA ==')

        status = 0
        return_value = []

        for line in data.split('\n'):
            if len(line) == 0:
                continue

            if line[0] == '#':
                continue

            values = line.split('=')
            if len(values) < 2:
                continue

            if values[0] == 'status':
                status = int(values[1])
            elif values[0] == 'auth_token':
                self.last_authtoken = values[1]

            return_value.append( (values[0], values[1]) )

        if status is None:
            raise ConnectionError

        if not status == 0:
            raise self._return_codes[status]

        return (status, return_value)

    def login(self):
        '''Login in the system'''
        arguments = {'g2_form[uname]': self.user,
                'g2_form[password]': self.password}
        logging.debug('User [%s] Password [%s]',
                self.user, self.password)

        (status, _) = self.request('login', '2.0', arguments)
        self.logged = True

    def albums(self, perms=True, prune=True):
        '''Request a list of albums'''
        request_args = {'no_perms': _yesno(not perms)}

        if prune:
            (status, data) = self.request(
                'fetch-albums-prune', '2.2', request_args)
        else:
            (status, data) = self.request(
                'fetch-albums', '2.0', request_args)

        last_album    = ''
        album_name    = None
        album_parent  = None
        album_title   = None

        for info in data:
            if info[0][-2:] != last_album:
                last_album = info[0][-2:]

                if album_name is not None and \
                        album_title is not None and \
                        album_parent is not None:
                    yield (int(album_name), album_title, int(album_parent))

                album_name   = None
                album_parent = None
                album_title  = None

            fields = info[0].split('.')
            if not fields[0] == 'album':
                continue

            if fields[1] == 'name':
                album_name = info[1]
            elif fields[1] == 'parent':
                album_parent = info[1]
            elif fields[1] == 'title':
                album_title = info[1]

        if album_name is not None and \
                album_title is not None and \
                album_parent is not None:
            yield (int(album_name), album_title, int(album_parent))

    def new_album(self, album_parent, album_name, album_title):
        '''Create a new album'''
        (status, data) = self.request('new-album', '2.1', {
            'g2_form[set_albumName]': str(album_parent),
            'g2_form[newAlbumName]': album_name,
            'g2_form[newAlbumTitle]': album_title,
            'g2_form[newAlbumDesc]': ''})

        for info in data:
            if info[0] == 'album_name':
                logging.debug('Created album "%s"', info[1])
                return int(info[1])

        return None

    def add_item(self, album, filename):
        '''Add an item to an album'''
        (status, _) = self.request('add-item', '2.0', {
            'g2_form[set_albumName]': str(album),
            'g2_userfile_name': filename
            },
            ('g2_userfile', filename)
            )

    def album_properties(self, album):
        '''Fetch information on an album'''
        (status, data) = self.request('album-properties', '2.0', {
                'g2_form[set_albumName]': str(album),
                })

        return data

    def album_images(self, album, albums_too=True,
                           random=False, limit=None,
                           extra_fields=True):
        '''Fetch album images'''
        request_args = {
            'g2_form[set_albumName]': str(album),
            'g2_form[albums_too]': _yesno(albums_too),
            'g2_form[extrafields]': _yesno(extra_fields),
            }
        if random and limit is not None:
            request_args['g2_form[random]'] = _yesno(random)
            request_args['g2_form[limit]'] = str(limit)

        (status, data) = self.request(
            'fetch-album-images', '2.4', request_args)

        meta = {}
        imagemap = {}
        images = []
        for key, value in data:
            if key.startswith('image.'):
                key, index = key[6:].rsplit('.', 1)
                index = int(index)
                if index not in imagemap:
                    imagemap[index] = {key: value}
                    images.append(imagemap[index])
                else:
                    imagemap[index][key] = value
            else:
                meta[key] = value

        return (meta, images)

    def image_properties(self, image):
        '''Fetch image properties'''
        (status, data) = self.request('image-properties', '***', {
                'id': str(image),
                })

        return data

    def no_op(self):
        '''Check the remote site is operational'''
        (status, data) = self.request('no-op', '***', {})
        return data
